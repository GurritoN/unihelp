CREATE OR REPLACE PROCEDURE create_task(title_ IN VARCHAR2, note_ IN VARCHAR2, user_id_ IN NUMBER,
            tag_id_ IN NUMBER, planner_id_ IN NUMBER, parent_task_id_ IN NUMBER,
            time_start_ IN VARCHAR2, time_end_ IN VARCHAR2, time_create_ IN VARCHAR2,
            time_update_ IN VARCHAR2, is_event_ IN NUMBER, priority_ IN NUMBER, status_ IN NUMBER) IS
BEGIN
  enq('INSERT INTO Task (title, note, user_id, tag_id, planner_id, parent_task_id, time_start, time_end, time_create, time_update, is_event, priority, status) VALUES (''' || title_ || ''', ''' || note_ || ''', ' || user_id_ || ', ' || tag_id_ || ', ' || planner_id_ || ', ' || parent_task_id_ || ', ''' || time_start_ || ''', ''' || time_end_ || ''', ''' || time_create_ || ''', ''' || time_update_ || ''', ' || is_event_ || ', ' || priority_ || ', ' || status_ || ')');
END;
  
  grant execute on create_task to public;

CREATE OR REPLACE PROCEDURE delete_task(id_ IN NUMBER) IS
BEGIN
  enq('DELETE FROM TASK WHERE id = ' || id_);
END;
  
  grant execute on delete_task to public;

CREATE OR REPLACE PROCEDURE update_task(id_ IN NUMBER, title_ IN VARCHAR2, note_ IN VARCHAR2, user_id_ IN NUMBER,
            tag_id_ IN NUMBER, planner_id_ IN NUMBER, parent_task_id_ IN NUMBER,
            time_start_ IN VARCHAR2, time_end_ IN VARCHAR2, time_create_ IN VARCHAR2,
            time_update_ IN VARCHAR2, is_event_ IN NUMBER, priority_ IN NUMBER, status_ IN NUMBER) IS
BEGIN
  enq('UPDATE Task SET title=''' || title_ || ''', note=''' || note_ || ''', user_id=' || user_id_ || ', tag_id=' || tag_id_ || ', planner_id=' || planner_id_ || ', parent_task_id=' || parent_task_id_ || ', time_start=''' || time_start_ || ''', time_end=''' || time_end_ || ''', time_create=''' || time_create_ || ''', time_update=''' || time_update_ || ''', is_event=' || is_event_ || ', priority=' || priority_ || ', status=' || status_ || ' WHERE id=' || id_);
END;
  
  grant execute on update_task to public;

CREATE OR REPLACE FUNCTION get_task(id_ IN NUMBER) RETURN SYS_REFCURSOR IS
  c1 SYS_REFCURSOR;
BEGIN
  OPEN c1 FOR
  SELECT * FROM TASK
  WHERE id = id_;
  RETURN c1;
END;
  
  grant execute on get_task to public;

CREATE OR REPLACE FUNCTION get_tasks_by_user(user_id_ IN NUMBER) RETURN SYS_REFCURSOR IS
  c1 SYS_REFCURSOR;
BEGIN
  OPEN c1 FOR
  SELECT * FROM TASK
  WHERE user_id = user_id_;
  RETURN c1;
END;
  
  grant execute on get_tasks_by_user to public;

CREATE OR REPLACE FUNCTION get_tasks_by_status(user_id_ IN NUMBER, status_ IN NUMBER) RETURN SYS_REFCURSOR IS
  c1 SYS_REFCURSOR;
BEGIN
  OPEN c1 FOR
  SELECT * FROM TASK
    WHERE user_id = user_id_ AND status=status_;
  RETURN c1;
END;
  
  grant execute on get_tasks_by_status to public;

CREATE OR REPLACE FUNCTION get_tasks_by_parent(parent_id_ IN NUMBER) RETURN SYS_REFCURSOR IS
  c1 SYS_REFCURSOR;
BEGIN
  OPEN c1 FOR
  SELECT * FROM TASK
    WHERE PARENT_TASK_ID = parent_id_;
  RETURN c1;
END;
  
  grant execute on get_tasks_by_parent to public;

CREATE OR REPLACE FUNCTION get_tasks_by_planner(user_id_ IN NUMBER, planner_id_ IN NUMBER) RETURN SYS_REFCURSOR IS
  c1 SYS_REFCURSOR;
BEGIN
  OPEN c1 FOR
  SELECT * FROM TASK
    WHERE user_id = user_id_ AND PLANNER_ID=planner_id_;
  RETURN c1;
END;
  
  grant execute on get_tasks_by_planner to public;

CREATE OR REPLACE FUNCTION get_active_tasks(user_id_ IN NUMBER) RETURN SYS_REFCURSOR IS
  c1 SYS_REFCURSOR;
BEGIN
  OPEN c1 FOR
  SELECT * FROM TASK
    WHERE user_id = user_id_ AND STATUS<>3 AND STATUS<>4;
  RETURN c1;
END;
  
  grant execute on get_active_tasks to public;
