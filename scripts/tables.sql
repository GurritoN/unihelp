CREATE TABLE Tag(
	id number GENERATED ALWAYS AS IDENTITY (START WITH 1 INCREMENT BY 1) NOT NULL primary key,
	name varchar2(1000) not null,
	user_id number
);
/
grant select on Tag to public;
/
CREATE TABLE Task(
	id number GENERATED ALWAYS AS IDENTITY (START WITH 1 INCREMENT BY 1) NOT NULL primary key,
	title varchar2(1000) not null,
	note varchar2(1000) default '',
	time_start varchar2(1000),
	time_end varchar2(1000),
	time_create varchar2(1000) not null,
	time_update varchar2(1000) not null,
	user_id number,
	planner_id number,
	parent_task_id number,
	tag_id number,
	is_event number default 0,
	priority number default 1,
	status number default 0,
	foreign key (tag_id) references Tag(id)
);
/
grant select on Task to public;
/
CREATE TABLE Planner(
	id number GENERATED ALWAYS AS IDENTITY (START WITH 1 INCREMENT BY 1) NOT NULL primary key,
	task_id number, 
	user_id number,
	interval number,
	last_repeat varchar2(1000) not null,
	foreign key (task_id) references Task(id)
	);
/
grant select on Planner to public;
/
CREATE TABLE Notification(
	id number GENERATED ALWAYS AS IDENTITY (START WITH 1 INCREMENT BY 1) NOT NULL primary key,
	title varchar2(1000) not null,
	task_id number, 
	user_id number,
	work_start_time varchar2(1000) not null,
	state number default 0,
	foreign key (task_id) references Task(id)
);
/
grant select on Notification to public;
/
CREATE TABLE ReadUserRight(
	task_id number, 
	user_id number,
	foreign key (task_id) references Task(id)
);
/
grant select on ReadUserRight to public;
/
CREATE TABLE WriteUserRight(
	task_id number, 
	user_id number,
	foreign key (task_id) references Task(id)
);
/
grant select on WriteUserRight to public;
/
CREATE TABLE Logs(
	username varchar2(1000),
	message varchar2(1000),
	time timestamp
);
/
grant select on Logs to public;
/