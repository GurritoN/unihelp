CREATE OR REPLACE PROCEDURE create_tag (name_ IN VARCHAR2, user_id_ IN NUMBER) AS
BEGIN
  enq('INSERT INTO Tag(name, user_id) VALUES(''' || name_ || ''', ' || user_id_ || ')');
END;

  grant execute on create_tag to public;

CREATE OR REPLACE PROCEDURE delete_tag (id_ IN NUMBER) AS
BEGIN
  enq('DELETE FROM Tag WHERE id = ' || id_);
END;

  grant execute on delete_tag to public;

CREATE OR REPLACE PROCEDURE update_tag (name_ IN VARCHAR2, id_ IN NUMBER) AS
BEGIN
  enq('UPDATE Tag SET name=''' || name_ || ''' WHERE id=' || id_);
END;

  grant execute on update_tag to public;

CREATE OR REPLACE FUNCTION get_tag (id_ IN NUMBER) RETURN SYS_REFCURSOR IS
  c1 SYS_REFCURSOR;
BEGIN
  OPEN c1 FOR
  SELECT * FROM Tag
  WHERE id = id_;
  RETURN c1;
END;

  grant execute on get_tag to public;

CREATE OR REPLACE FUNCTION get_all_tags (user_id_ IN NUMBER) RETURN SYS_REFCURSOR IS
  c1 SYS_REFCURSOR;
BEGIN
  OPEN c1 FOR
  SELECT * FROM Tag
  WHERE user_id = user_id_;
  RETURN c1;
END;

  grant execute on get_all_tags to public;
