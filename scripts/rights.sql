CREATE OR REPLACE FUNCTION check_read_rights(user_id_ IN NUMBER, task_id_ IN NUMBER) RETURN BOOLEAN IS
  c NUMBER;
BEGIN
  SELECT COUNT(*) INTO c FROM READUSERRIGHT
    WHERE user_id=user_id_ AND task_id=task_id_;
  RETURN c>0;
END;

grant execute on check_read_rights to public;

CREATE OR REPLACE PROCEDURE add_reader(user_id_ IN NUMBER, task_id_ IN NUMBER) IS
BEGIN
  enq('INSERT INTO ReadUserRight (user_id, task_id) VALUES (' || user_id_ || ', ' || task_id_ || ')');
END;

grant execute on add_reader to public;

CREATE OR REPLACE FUNCTION get_read_rights(task_id_ IN NUMBER) RETURN SYS_REFCURSOR IS
  c1 SYS_REFCURSOR;
BEGIN
  OPEN c1 FOR
  SELECT * FROM ReadUserRight
            WHERE task_id=task_id_;
  RETURN c1;
END;

grant execute on get_read_rights to public;

CREATE OR REPLACE PROCEDURE delete_reader(user_id_ IN NUMBER, task_id_ IN NUMBER) IS
BEGIN
  enq('DELETE from ReadUserRight WHERE user_id=' || user_id_ || ' and task_id=' || task_id_);
END;

grant execute on delete_reader to public;

CREATE OR REPLACE PROCEDURE delete_all_readers(task_id_ IN NUMBER) IS
BEGIN
  enq('DELETE from ReadUserRight WHERE task_id=' || task_id_);
END;

grant execute on delete_all_readers to public;

CREATE OR REPLACE FUNCTION get_read_tasks(user_id_ IN NUMBER) RETURN SYS_REFCURSOR IS
  c1 SYS_REFCURSOR;
BEGIN
  OPEN c1 FOR
  SELECT Task.* FROM Task
    INNER JOIN ReadUserRight ON Task.id=ReadUserRight.task_id
    WHERE ReadUserRight.user_id=user_id_;
  RETURN c1;
END;

grant execute on get_read_tasks to public;

CREATE OR REPLACE FUNCTION check_write_rights(user_id_ IN NUMBER, task_id_ IN NUMBER) RETURN BOOLEAN IS
  c NUMBER;
BEGIN
  SELECT COUNT(*) INTO c FROM WRITEUSERRIGHT
    WHERE user_id=user_id_ AND task_id=task_id_;
  RETURN c>0;
END;

grant execute on check_write_rights to public;

CREATE OR REPLACE PROCEDURE add_writer(user_id_ IN NUMBER, task_id_ IN NUMBER) IS
BEGIN
  enq('INSERT INTO WriteUserRight (user_id, task_id) VALUES (' || user_id_ || ', ' || task_id_ || ')');
END;

grant execute on add_writer to public;

CREATE OR REPLACE FUNCTION get_write_rights(task_id_ IN NUMBER) RETURN SYS_REFCURSOR IS
  c1 SYS_REFCURSOR;
BEGIN
  OPEN c1 FOR
  SELECT * FROM WriteUserRight
            WHERE task_id=task_id_;
  RETURN c1;
END;

grant execute on get_write_rights to public;

CREATE OR REPLACE PROCEDURE delete_writer(user_id_ IN NUMBER, task_id_ IN NUMBER) IS
BEGIN
  enq('DELETE from WriteUserRight WHERE user_id=' || user_id_ || ' and task_id=' || task_id_);
END;

grant execute on delete_writer to public;

CREATE OR REPLACE PROCEDURE delete_all_writers(task_id_ IN NUMBER) IS
BEGIN
  enq('DELETE from WriteUserRight WHERE task_id=' || task_id_);
END;

grant execute on delete_all_writers to public;

CREATE OR REPLACE FUNCTION get_write_tasks(user_id_ IN NUMBER) RETURN SYS_REFCURSOR IS
  c1 SYS_REFCURSOR;
BEGIN
  OPEN c1 FOR
  SELECT Task.* FROM Task
    INNER JOIN WriteUserRight ON Task.id=WriteUserRight.task_id
    WHERE WriteUserRight.user_id=user_id_;
  RETURN c1;
END;

grant execute on get_write_tasks to public;