create or replace type queryqueuetype is object
(
  query varchar2(1000),
  username varchar2(1000)
);
begin
  dbms_aqadm.create_queue_table(
    queue_table        => 'queryqueuetable',
    queue_payload_type => 'queryqueuetype'
  );
  dbms_aqadm.create_queue(
    queue_name  => 'queryqueue',
    queue_table => 'queryqueuetable'
  );
  dbms_aqadm.start_queue(
    queue_name  => 'queryqueue'
  );
end;

create or replace procedure enq(query varchar2) is
  l_msg_id raw(32767);
  sid number;
  l_enq_opt dbms_aq.enqueue_options_t;
  l_msg_prop dbms_aq.message_properties_t;
begin
  l_enq_opt.visibility := dbms_aq.IMMEDIATE;
  dbms_aq.enqueue(
    queue_name         => 'queryqueue',
    enqueue_options    => l_enq_opt,
    message_properties => l_msg_prop,
    payload            => queryqueuetype(query, sys_context('userenv', 'current_user')),
    msgid              => l_msg_id
  );
end;

create or replace procedure deq is
  l_msg_id raw(16);
  l_deq_opt dbms_aq.dequeue_options_t;
  l_msg_prop dbms_aq.message_properties_t;
  l_payload queryqueuetype;
  count_ number := null;
  errm varchar2(1000);
  errc number;
begin
  LOOP
    l_deq_opt.wait := dbms_aq.NO_WAIT;
    dbms_aq.dequeue(
      queue_name         => 'queryqueue',
      dequeue_options    => l_deq_opt,
      message_properties => l_msg_prop,
      payload            => l_payload,
      msgid              => l_msg_id
    );
    INSERT INTO LOGS VALUES(l_payload.USERNAME, l_payload.QUERY, systimestamp);
    EXECUTE IMMEDIATE l_payload.QUERY;
    INSERT INTO LOGS VALUES(l_payload.USERNAME, 'SUCCESS', systimestamp);
  END LOOP;
EXCEPTION
  WHEN OTHERS THEN
    errc := SQLCODE;
    IF errc <> -25228 THEN
      errm := SQLERRM;
      INSERT INTO LOGS VALUES(l_payload.USERNAME, errm, systimestamp);
    end if;
end;