CREATE OR REPLACE PROCEDURE create_notification (title_ varchar2, state_ number, user_id_ number, task_id_ number, work_start_time_ VARCHAR2) AS
BEGIN
  enq('INSERT INTO Notification (title, state, user_id, task_id, work_start_time) VALUES (''' || title_ || ''', ' || state_ || ', ' || user_id_ ||', ' || task_id_ || ', ''' || work_start_time_ || ''')');
END;

grant execute on create_notification to public;

CREATE OR REPLACE PROCEDURE delete_notification (id_ IN NUMBER) AS
BEGIN
  enq('DELETE FROM NOTIFICATION WHERE id = ' || id_);
END;

grant execute on delete_notification to public;

CREATE OR REPLACE PROCEDURE update_notification (id_ number, title_ varchar2, state_ number, user_id_ number, task_id_ number, work_start_time_ VARCHAR2) AS
BEGIN
  enq('UPDATE Notification SET title=''' || title_ || ''', state=' || state_ || ', ' || 'user_id=' || user_id_ ||', ' || 'task_id=' || task_id_ || ', work_start_time=''' || work_start_time_ || ''' WHERE id=' || id_);
END;

grant execute on UPDATE_NOTIFICATION to public;

CREATE OR REPLACE FUNCTION get_notification (id_ IN NUMBER) RETURN SYS_REFCURSOR IS
  c1 SYS_REFCURSOR;
BEGIN
  OPEN c1 FOR
  SELECT * FROM NOTIFICATION
  WHERE id = id_;
  RETURN c1;
END;

grant execute on GET_NOTIFICATION to public;

CREATE OR REPLACE FUNCTION get_user_notifications (user_id_ IN NUMBER) RETURN SYS_REFCURSOR IS
  c1 SYS_REFCURSOR;
BEGIN
  OPEN c1 FOR
  SELECT * FROM NOTIFICATION
  WHERE user_id = user_id_;
  RETURN c1;
END;

grant execute on GET_USER_NOTIFICATIONS to public;

CREATE OR REPLACE FUNCTION get_user_notifications_by_state (user_id_ IN NUMBER, state_ IN NUMBER) RETURN SYS_REFCURSOR IS
  c1 SYS_REFCURSOR;
BEGIN
  OPEN c1 FOR
  SELECT * FROM NOTIFICATION
  WHERE user_id = user_id_ AND state = state_;
  RETURN c1;
END;

grant execute on get_user_notifications_by_state to public;

CREATE OR REPLACE FUNCTION get_all_notifications_by_state (state_ IN NUMBER) RETURN SYS_REFCURSOR IS
  c1 SYS_REFCURSOR;
BEGIN
  OPEN c1 FOR
  SELECT * FROM NOTIFICATION
  WHERE state = state_;
  RETURN c1;
END;

grant execute on get_all_notifications_by_state to public;