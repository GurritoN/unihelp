CREATE OR REPLACE PROCEDURE create_planner(user_id_ IN NUMBER, task_id_ IN NUMBER, interval_ IN NUMBER, last_repeat_ IN VARCHAR2) IS
BEGIN
  enq('INSERT INTO Planner (user_id, task_id, interval, last_repeat) VALUES (' || user_id_ || ', ' || task_id_ || ', ' || interval_ || ', ''' || last_repeat_ || ''')');
END;

  grant execute on create_planner to public;

CREATE OR REPLACE PROCEDURE delete_planner(id_ IN NUMBER) IS
BEGIN
  enq('DELETE FROM PLANNER WHERE id = ' || id_);
END;

  grant execute on delete_planner to public;

CREATE OR REPLACE PROCEDURE update_planner(id_ IN NUMBER, interval_ IN NUMBER, last_repeat_ IN VARCHAR2) IS
BEGIN
  enq('UPDATE Planner SET interval=' || interval_ || ', last_repeat=' || last_repeat_ || ' WHERE id=' || id_);
END;

  grant execute on update_planner to public;

CREATE OR REPLACE FUNCTION get_planner(id_ IN NUMBER) RETURN SYS_REFCURSOR IS
  c1 SYS_REFCURSOR;
BEGIN
  OPEN c1 FOR
  SELECT * FROM PLANNER
  WHERE id = id_;
  RETURN c1;
END;

  grant execute on get_planner to public;

CREATE OR REPLACE FUNCTION get_planners_by_user(user_id_ IN NUMBER) RETURN SYS_REFCURSOR IS
  c1 SYS_REFCURSOR;
BEGIN
  OPEN c1 FOR
  SELECT * FROM PLANNER
  WHERE user_id = user_id_;
  RETURN c1;
END;

  grant execute on get_planners_by_user to public;

CREATE OR REPLACE FUNCTION get_all_planners RETURN SYS_REFCURSOR IS
  c1 SYS_REFCURSOR;
BEGIN
  OPEN c1 FOR
  SELECT * FROM PLANNER;
  RETURN c1;
END;

  grant execute on get_all_planners to public;