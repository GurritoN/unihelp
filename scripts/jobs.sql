BEGIN
    DBMS_SCHEDULER.CREATE_JOB(
      job_name => 'queuer',
      job_type => 'stored_procedure',
      job_action => 'deq',
      start_date => SYSTIMESTAMP,
      repeat_interval => 'FREQ=SECONDLY',
      enabled => false
      );
    DBMS_SCHEDULER.ENABLE('queuer');
    DBMS_SCHEDULER.RUN_JOB('queuer');
end;

BEGIN
  dbms_scheduler.Drop_job('queuer');
end;