import datetime
import dateparser

from django.shortcuts import render, redirect
from django.conf import settings

from django.contrib.auth import authenticate, login as auth_login
from django.contrib.auth.models import User
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.decorators import login_required

from .forms import (
    TagForm,
    LoginForm,
    TaskForm,
    RepeatTaskForm,
    PlannerForm,
    MessageForm,
    NotificationForm,
    SubtaskForm)
from .templatetags.template_tags import (
    get_status_tag,
    get_priority_tag)

from unihelplib.actions import Actions
from unihelplib.exceptions.exceptions import InvalidTaskTimeError
from unihelplib.models.models import (
    State,
    Status,
    Priority,
    Planner,
    Notification,
    Tag,
    Task)


def home(request):
    username = 'someone'
    if request.user.is_authenticated:
        username = request.user.username
    return render(
        request,
        'home.html',
        {'username': username,
         'await_notifications':
             await_notifications(request.user.id),
         'await_notifications_count':
             len(await_notifications(request.user.id))})


def await_notifications(user_id):
    actions = Actions(user_id, settings.UNIHELP_DATABASE)
    if user_id is not None:
        return actions.search_notification_by_state(State.AWAIT)
    return []


def signup(request):
    if request.method == 'POST':
        form = UserCreationForm(request.POST)
        if form.is_valid():
            form.save()
            username = form.cleaned_data.get('username')
            password = form.cleaned_data.get('password1')
            user = authenticate(username=username, password=password)
            auth_login(request, user)
            return redirect('unihelp:home')
        else:
            return render(request, 'users/signup.html', {'form': form})
    else:
        form = UserCreationForm()
    return render(request, 'users/signup.html', {'form': form})


def login(request):
    if request.method == 'POST':
        form = LoginForm(request.POST)
        if form.is_valid():
            username = form.cleaned_data.get('username')
            password = form.cleaned_data.get('password')

            try:
                user = authenticate(username=username, password=password)
                auth_login(request, user=user)
                return redirect('unihelp:home')
            except Exception:
                return render(request, 'users/login.html', {'form': form, 'error': "User doesn't exist."})
        else:
            return render(request, 'users/login.html', {'form': form})
    else:
        form = LoginForm()
    return render(request, 'users/login.html', {'form': form})


def users(request):
    all_users = User.objects.all()
    return render(request,
                  'users/index.html',
                  {'users': all_users,
                   'user': request.user,
                   'nav_bar': 'users',
                   'await_notifications':
                       await_notifications(request.user.id),
                   'await_notifications_count':
                       len(await_notifications(request.user.id))})


def send_notification(request, user_id):
    if request.method == 'POST':
        form = MessageForm(user_id, request.POST)

        if form.is_valid():
            title = (form.cleaned_data['title']
                     + "\n(Message from {})".format(request.user.username))
            work_start_time = form.data['work_start_time']
            actions = Actions(user_id, settings.UNIHELP_DATABASE)

            notification = Notification(
                title=title,
                work_start_time=work_start_time,
                user_id=user_id,
                task_id=None)

            actions.add_notification(notification)
            return redirect('unihelp:users')
    else:
        form = MessageForm(user_id)
    return render(request,
                  'users/message.html',
                  {'form': form,
                   'user': request.user,
                   'nav_bar': 'notifications',
                   'await_notifications':
                       await_notifications(request.user.id),
                   'await_notifications_count':
                       len(await_notifications(request.user.id))})


def delete_user(request, user_id):
    if request.method == 'POST':
        return redirect('unihelp:users')
    user = User.objects.get(id=user_id)
    # user.is_active = False
    # user.save()
    user.delete()
    return render(request,
                  'home.html',
                  {'user': request.user,
                   'nav_bar': 'notifications',
                   'await_notifications':
                       await_notifications(request.user.id),
                   'await_notifications_count':
                       len(await_notifications(request.user.id))})


def check_planners(action):
    def wrapper(request, *args, **kwargs):
        actions = Actions(request.user.id, settings.UNIHELP_DATABASE)
        actions.progress_planners()
        return action(request, *args, **kwargs)
    return wrapper


def check_notifications(action):
    def wrapper(request, *args, **kwargs):
        actions = Actions(request.user.id, settings.UNIHELP_DATABASE)
        actions.notification_mediator.progress()
        return action(request, *args, **kwargs)
    return wrapper


@login_required
def tags(request):
    actions = Actions(request.user.id, settings.UNIHELP_DATABASE)
    user_tags = actions.search_user_tags()
    return render(request,
                  'tags/index.html',
                  {'tags': user_tags,
                   'user': request.user,
                   'nav_bar': 'tags',
                   'await_notifications':
                       await_notifications(request.user.id),
                   'await_notifications_count':
                       len(await_notifications(request.user.id))})


@login_required
def create_tag(request):
    if request.method == 'POST':
        form = TagForm(request.POST)
        if form.is_valid():
            name = form.cleaned_data['name']
            tag = Tag(name=name)
            actions = Actions(request.user.id, settings.UNIHELP_DATABASE)
            actions.add_tag(tag)
            return redirect('unihelp:tags')
    else:
        form = TagForm()
    return render(request,
                  'tags/new.html',
                  {'form': form,
                   'user': request.user,
                   'nav_bar': 'tags',
                   'await_notifications':
                       await_notifications(request.user.id),
                   'await_notifications_count':
                       len(await_notifications(request.user.id))})


@login_required
def edit_tag(request, tag_id):
    actions = Actions(request.user.id, settings.UNIHELP_DATABASE)
    tag = actions.search_tag(tag_id)

    if tag is None:
        return redirect('unihelp:tags')

    if request.method == 'POST':
        form = TagForm(request.POST)

        if form.is_valid():
            tag.name = form.cleaned_data['name']
            actions.update_tag(tag)
            return redirect('unihelp:tags')
    else:
        form = TagForm(initial={'name': tag.name})
        return render(request,
                      'tags/edit.html',
                      {'form': form,
                       'user': request.user,
                       'nav_bar': 'tags',
                       'await_notifications':
                           await_notifications(request.user.id),
                       'await_notifications_count':
                           len(await_notifications(request.user.id))})


@login_required
def delete_tag(request, tag_id):
    if request.method == 'POST':
        actions = Actions(request.user.id, settings.UNIHELP_DATABASE)
        actions.delete_tag(tag_id)
    return redirect('unihelp:tags')


@login_required
@check_planners
def search_active_tasks(request):
    actions = Actions(request.user.id, settings.UNIHELP_DATABASE)
    # tasks = actions.filter_tasks(
    #    (Task.status != Status.ARCHIVED.value)
    #    & (Task.user_id == request.user.id)
    #    & (Task.status != Status.REPEAT.value))
    tasks = actions.search_active_tasks()  # !!!
    return render(request,
                  'tasks/index.html',
                  {'tasks': tasks,
                   'user': request.user,
                   'nav_bar': 'tasks',
                   'header': 'Tasks',
                   'await_notifications':
                       await_notifications(request.user.id),
                   'await_notifications_count':
                       len(await_notifications(request.user.id))})


@login_required
@check_planners
def search_tasks_by_tag(request, tag_id):
    actions = Actions(request.user.id, settings.UNIHELP_DATABASE)
    # tasks = actions.filter_tasks(
    #    (Task.tag_id == int(tag_id))
    #    & (Task.user_id == request.user.id)
    #    & (Task.status != Status.ARCHIVED.value)
    #    & (Task.status != Status.REPEAT.value))
    tasks = actions.search_tasks_by_tag(tag_id)
    tag = actions.search_tag(tag_id)
    query = '?tag={}'.format(id)
    return render(request,
                  'tasks/index.html',
                  {'tasks': tasks,
                   'user': request.user,
                   'nav_bar': 'tasks',
                   'header': 'Tasks with tag "{}"'.format(tag.name),
                   'query': query,
                   'await_notifications':
                       await_notifications(request.user.id),
                   'await_notifications_count':
                       len(await_notifications(request.user.id))})


@login_required
@check_planners
def search_tasks_by_status(request, status_value):
    actions = Actions(request.user.id, settings.UNIHELP_DATABASE)
    # tasks = actions.filter_tasks(
    #    (Task.status == int(status_value))
    #    & (Task.user_id == request.user.id))
    tasks = actions.search_tasks_by_status_(status_value)
    query = '?status={}'.format(id)
    return render(
        request,
        'tasks/index.html',
        {'tasks': tasks,
         'user': request.user,
         'nav_bar': 'tasks',
         'header': 'Tasks by status <span class="badge badge-'
                   + get_status_tag(int(status_value))
                   + '">{}</span>'.format(Status(int(status_value)).name),
         'query': query,
         'status_value': status_value,
         'await_notifications':
             await_notifications(request.user.id),
         'await_notifications_count':
             len(await_notifications(request.user.id))})


@login_required
@check_planners
def search_tasks_by_priority(request, priority_value):
    actions = Actions(request.user.id, settings.UNIHELP_DATABASE)
    # tasks = actions.filter_tasks(
    #    (Task.priority == int(priority_value))
    #    & (Task.user_id == request.user.id)
    #    & (Task.status != Status.ARCHIVED.value)
    #    & (Task.status != Status.REPEAT.value))
    tasks = actions.search_tasks_by_priority(priority_value)
    query = '?priority={}'.format(priority_value)
    return render(
        request,
        'tasks/index.html',
        {'tasks': tasks,
         'user': request.user,
         'nav_bar': 'tasks',
         'header': 'Tasks by priority <span class="badge badge-'
                   + get_priority_tag(int(priority_value))
                   + '">{}</span>'.format(Priority(int(priority_value)).name),
         'query': query,
         'await_notifications':
             await_notifications(request.user.id),
         'await_notifications_count':
             len(await_notifications(request.user.id))})


@login_required
@check_planners
def search_tasks_by_planner(request, planner_id):
    actions = Actions(request.user.id, settings.UNIHELP_DATABASE)
    # tasks = actions.filter_tasks(
    #    (Task.planner_id == int(planner_id))
    #    & (Task.user_id == request.user.id))
    tasks = actions.search_tasks_by_planner(planner_id)
    return render(request,
                  'tasks/index.html',
                  {'tasks': tasks,
                   'user': request.user,
                   'nav_bar': 'tasks',
                   'header': 'Tasks created by planner with ID {}'.format(id),
                   'await_notifications':
                       await_notifications(request.user.id),
                   'await_notifications_count':
                       len(await_notifications(request.user.id))})


@login_required
@check_planners
def user_tasks(request):
    actions = Actions(request.user.id, settings.UNIHELP_DATABASE)
    tasks = actions.search_user_tasks()
    return render(request,
                  'tasks/index.html',
                  {'tasks': tasks,
                   'user': request.user,
                   'nav_bar': 'tasks',
                   'header': 'My tasks',
                   'foreign_tag': True,
                   'await_notifications':
                       await_notifications(request.user.id),
                   'await_notifications_count':
                       len(await_notifications(request.user.id))})


@login_required
@check_planners
def search_can_read_tasks(request):
    actions = Actions(request.user.id, settings.UNIHELP_DATABASE)
    tasks = actions.search_user_can_read_tasks()
    return render(request,
                  'tasks/index.html',
                  {'tasks': tasks,
                   'user': request.user,
                   'nav_bar': 'tasks',
                   'header': 'My tasks with reader rights',
                   'view': 'can_read',
                   'foreign_tag': True,
                   'await_notifications':
                       await_notifications(request.user.id),
                   'await_notifications_count':
                       len(await_notifications(request.user.id))})


@login_required
@check_planners
def search_can_write_tasks(request):
    actions = Actions(request.user.id, settings.UNIHELP_DATABASE)
    tasks = actions.search_user_can_write_tasks()
    return render(request,
                  'tasks/index.html',
                  {'tasks': tasks,
                   'user': request.user,
                   'nav_bar': 'tasks',
                   'header': 'My tasks with writer rights',
                   'view': 'can_write',
                   'foreign_tag': True,
                   'await_notifications':
                       await_notifications(request.user.id),
                   'await_notifications_count':
                       len(await_notifications(request.user.id))})


@login_required
@check_planners
def create_task(request):
    if request.method == 'POST':
        form = TaskForm(request.user.id, request.POST)
        if form.is_valid():
            title = form.cleaned_data['title']
            note = form.cleaned_data['note']
            tag_id = form.cleaned_data['tag']
            if not tag_id:
                tag_id = None
            priority = form.cleaned_data['priority']
            status = form.cleaned_data['status']
            is_event = form.cleaned_data['event']
            time_start = form.cleaned_data['time_start']
            time_end = form.cleaned_data['time_end']

            task = Task(
                title=title,
                note=note,
                tag_id=tag_id,
                priority=priority,
                status=status,
                is_event=is_event,
                time_start=time_start,
                time_end=time_end,
                parent_task_id=None)
            actions = Actions(request.user.id, settings.UNIHELP_DATABASE)

            try:
                task.id = actions.add_task(task).id
            except InvalidTaskTimeError:
                form.add_error(
                    'start_time',
                    'Time is invalid: start time is more than end time')

                return render(
                    request,
                    'tasks/new.html',
                    {'form': form,
                     'user': request.user,
                     'nav_bar': 'tasks',
                     'await_notifications':
                         await_notifications(request.user.id),
                     'await_notifications_count':
                         len(await_notifications(request.user.id))})

            readers = form.cleaned_data['readers']
            writers = form.cleaned_data['writers']
            actions.delete_all_readers(task.id)

            if readers is not None:
                ids = [user.id for user in readers]
                for user_id in ids:
                    actions.add_reader(user_id, task.id)
            actions.delete_all_writers(task.id)

            if writers is not None:
                ids = [user.id for user in writers]
                for user_id in ids:
                    actions.add_writer(user_id, task.id)

            return redirect('unihelp:tasks')
    else:
        status = Status.CREATED.value,
        priority = Priority.MID.value
        tag = None

        if request.GET.get('tag') is not None:
            tag = request.GET.get('tag')
        if request.GET.get('status') is not None:
            status = request.GET.get('status')
        if request.GET.get('priority') is not None:
            priority = request.GET.get('priority')

        form = TaskForm(request.user.id)
        form.fields["status"].initial = status
        form.fields["priority"].initial = priority

        if tag is not None:
            form.fields["tag"].initial = tag

    return render(request,
                  'tasks/new.html',
                  {'form': form,
                   'user': request.user,
                   'nav_bar': 'tasks',
                   'await_notifications':
                       await_notifications(request.user.id),
                   'await_notifications_count':
                       len(await_notifications(request.user.id))})


@login_required
@check_planners
def add_subtask(request, parent_id):
    if request.method == 'POST':
        form = SubtaskForm(request.user.id, request.POST)

        if form.is_valid():
            title = form.cleaned_data['title']
            note = form.cleaned_data['note']
            tag_id = form.cleaned_data['tag']

            if not tag_id:
                tag_id = None

            priority = form.cleaned_data['priority']
            status = form.cleaned_data['status']
            is_event = form.cleaned_data['event']
            time_start = form.cleaned_data['time_start']
            time_end = form.cleaned_data['time_end']

            task = Task(
                title=title,
                note=note,
                tag_id=tag_id,
                priority=priority,
                status=status,
                is_event=is_event,
                time_start=time_start,
                time_end=time_end,
                parent_task_id=parent_id)
            actions = Actions(request.user.id, settings.UNIHELP_DATABASE)

            try:
                task.id = actions.add_task(task).id
            except InvalidTaskTimeError:
                form.add_error(
                    'start_time',
                    'Time is invalid: start time is more than end time')

                return render(
                    request,
                    'tasks/new.html',
                    {'form': form,
                     'user': request.user,
                     'nav_bar': 'tasks',
                     'await_notifications':
                         await_notifications(request.user.id),
                     'await_notifications_count':
                         len(await_notifications(request.user.id))})

            readers = form.cleaned_data['readers']
            writers = form.cleaned_data['writers']
            actions.delete_all_readers(task.id)

            if readers is not None:
                ids = [user.id for user in readers]
                for user_id in ids:
                    actions.add_reader(user_id, task.id)

            actions.delete_all_writers(task.id)
            if writers is not None:
                ids = [user.id for user in writers]
                for user_id in ids:
                    actions.add_writer(user_id, task.id)

            return redirect('unihelp:tasks')
    else:
        status = Status.CREATED.value,
        priority = Priority.MID.value
        tag = None

        if request.GET.get('tag') is not None:
            tag = request.GET.get('tag')
        if request.GET.get('status') is not None:
            status = request.GET.get('status')
        if request.GET.get('priority') is not None:
            priority = request.GET.get('priority')

        form = SubtaskForm(request.user.id)
        form.fields["status"].initial = status
        form.fields["priority"].initial = priority

        if tag is not None:
            form.fields["tag"].initial = tag

    return render(request,
                  'tasks/subtask.html',
                  {'form': form,
                   'user': request.user,
                   'nav_bar': 'tasks',
                   'await_notifications':
                       await_notifications(request.user.id),
                   'await_notifications_count':
                       len(await_notifications(request.user.id))})


@login_required
@check_planners
def show_task(request, task_id):
    actions = Actions(request.user.id, settings.UNIHELP_DATABASE)
    task = actions.search_task(task_id)

    if task is None:
        return redirect('unihelp:tasks')

    tag = actions.search_tag(task.tag_id)
    parent_task_title = None

    if task.parent_task_id is not None:
        parent_task = actions.search_task(task.parent_task_id)
        if parent_task is not None:
            parent_task_title = parent_task.title

    subtasks = actions.search_subtasks(task.id, recursive=True)
    creator = User.objects.get(id=task.user_id)

    return render(request,
                  'tasks/show.html',
                  {'user': request.user,
                   'nav_bar': 'tasks',
                   'task': task,
                   'tag': tag,
                   'priority': Priority(task.priority).name,
                   'status': Status(task.status).name,
                   'parent_task_title': parent_task_title,
                   'subtasks': subtasks,
                   'creator': creator.username,
                   'await_notifications':
                       await_notifications(request.user.id),
                   'await_notifications_count':
                       len(await_notifications(request.user.id))})


@login_required
@check_planners
def edit_task(request, task_id):
    actions = Actions(request.user.id, settings.UNIHELP_DATABASE)
    task = actions.search_task(task_id)

    if task is None or not actions.check_user_is_writer(task_id):
        return redirect('unihelp:tasks')

    if request.method == 'POST':
        form = TaskForm(request.user.id, request.POST)

        if form.is_valid():
            task.title = form.cleaned_data['title']
            task.note = form.cleaned_data['note']
            task.tag_id = form.cleaned_data['tag']

            if not task.tag_id:
                task.tag_id = None

            task.priority = form.cleaned_data['priority']
            task.status = int(form.cleaned_data['status'])
            task.is_event = form.cleaned_data['event']
            task.time_start = form.cleaned_data['time_start']
            task.time_end = form.cleaned_data['time_end']

            try:
                actions.update_task(task)
            except InvalidTaskTimeError:
                form.add_error(
                    'start_time',
                    'Time is invalid: start time is more than end time')

                return render(
                    request,
                    'tasks/edit.html',
                    {
                        'form': form,
                        'user': request.user,
                        'nav_bar': 'tasks',
                        'await_notifications':
                            await_notifications(request.user.id),
                        'await_notifications_count':
                            len(await_notifications(request.user.id))})

            readers = form.cleaned_data['readers']
            writers = form.cleaned_data['writers']
            actions.delete_all_readers(task.id)

            if readers is not None:
                ids = [user.id for user in readers]
                for user_id in ids:
                    actions.add_reader(user_id, task.id)
            actions.delete_all_writers(task.id)

            if writers is not None:
                ids = [user.id for user in writers]
                for user_id in ids:
                    actions.add_writer(user_id, task.id)

            return redirect('unihelp:tasks')
    else:
        readers_ids = actions.search_readers(task_id)
        writers_ids = actions.search_writers(task_id)
        readers = User.objects.filter(id__in=readers_ids)
        writers = User.objects.filter(id__in=writers_ids)

        form = TaskForm(
            request.user.id,
            initial={
                'title': task.title,
                'note': task.note,
                'status': task.status,
                'priority': task.priority,
                'tag': task.tag_id,
                'event': task.is_event,
                'time_start': task.time_start,
                'time_end': task.time_end,
                'readers': readers,
                'writers': writers})

    return render(request,
                  'tasks/edit.html',
                  {'form': form,
                   'user': request.user,
                   'nav_bar': 'tasks',
                   'await_notifications':
                       await_notifications(request.user.id),
                   'await_notifications_count':
                       len(await_notifications(request.user.id))})


@login_required
@check_planners
def update_task(request, task_id):
    actions = Actions(request.user.id, settings.UNIHELP_DATABASE)
    task = actions.search_task(task_id)

    if task is None or not actions.check_user_is_writer(task_id):
        return redirect('unihelp:tasks')

    if request.method == 'POST':
        task.status = int(request.POST.get('status'))
        task.priority = int(request.POST.get('priority'))
        actions.update_task(task)

    return redirect('unihelp:tasks')


@login_required
@check_planners
def delete_task(request, task_id):
    actions = Actions(request.user.id, settings.UNIHELP_DATABASE)
    if request.method == 'POST' and actions.check_user_is_writer(task_id):
        task = actions.search_task(task_id)
        task.status = Status.ARCHIVED.value
        actions.update_task(task)
    return redirect('unihelp:tasks')


@login_required
@check_planners
def delete_archived_task(request, task_id):
    actions = Actions(request.user.id, settings.UNIHELP_DATABASE)
    if request.method == 'POST' and actions.check_user_is_writer(task_id):
        actions.delete_task(task_id)
    return redirect('unihelp:tasks')


@login_required
@check_notifications
@check_planners
def notifications(request):
    actions = Actions(request.user.id, settings.UNIHELP_DATABASE)
    # tasks = actions.filter_tasks(
    #    (Task.time_start > datetime.datetime.now())
    #    & (Task.user_id == request.user.id))
    tasks = actions.search_future_tasks()
    return render(request,
                  'notifications/index.html',
                  {'tasks': tasks,
                   'user': request.user,
                   'nav_bar': 'notifications',
                   'await_notifications':
                       await_notifications(request.user.id),
                   'await_notifications_count':
                       len(await_notifications(request.user.id))})


@login_required
@check_notifications
def create_notification(request):
    if request.method == 'POST':
        form = NotificationForm(request.user.id, request.POST)
        actions = Actions(request.user.id, settings.UNIHELP_DATABASE)
        if form.is_valid():
            title = form.cleaned_data['title']
            task = form.cleaned_data['task']

            if not task:
                task = None
            else:
                task = actions.search_task(task)
            work_start_time = form.data['work_start_time']
            actions = Actions(request.user.id, settings.UNIHELP_DATABASE)

            notification = Notification(
                title=title,
                work_start_time=work_start_time,
                user_id=request.user.id,
                task_id=task.id if task is not None else None)
            actions.add_notification(notification)

            return redirect('unihelp:all_notifications')
    else:
        task = None
        if request.GET.get('task') is not None:
            task = request.GET.get('task')
        form = NotificationForm(request.user.id)
        if task is not None:
            form.fields["task"].initial = task

    return render(request,
                  'notifications/new.html',
                  {'form': form,
                   'user': request.user,
                   'nav_bar': 'notifications',
                   'await_notifications':
                       await_notifications(request.user.id),
                   'await_notifications_count':
                       len(await_notifications(request.user.id))})


@login_required
@check_notifications
def edit_notification(request, notification_id):
    actions = Actions(request.user.id, settings.UNIHELP_DATABASE)
    notification = actions.search_notification(notification_id)

    if notification is None:
        return redirect('unihelp:all_notifications')

    if request.method == 'POST':
        form = NotificationForm(request.user.id, request.POST)
        work_start_time = form.data['work_start_time']

        if form.is_valid():
            notification.title = form.cleaned_data['title']
            notification.work_start_time = work_start_time
            actions.update_notification(notification)

            return redirect('unihelp:all_notifications')
    else:
        form = NotificationForm(
            initial={'title': notification.title,
                     'work_start_time': notification.work_start_time})

    return render(request,
                  'notifications/edit.html',
                  {'form': form,
                   'user': request.user,
                   'nav_bar': 'notifications',
                   'await_notifications':
                       await_notifications(request.user.id),
                   'await_notifications_count':
                       len(await_notifications(request.user.id))})


@login_required
@check_notifications
def search_all_notifications(request):
    actions = Actions(request.user.id, settings.UNIHELP_DATABASE)
    all_notifications = actions.search_all_notification()
    return render(request,
                  'notifications/index.html',
                  {'notifications': all_notifications,
                   'user': request.user,
                   'nav_bar': 'notifications',
                   'header': 'All notifications',
                   'await_notifications':
                       await_notifications(request.user.id),
                   'await_notifications_count':
                       len(await_notifications(request.user.id))})


@login_required
@check_notifications
def search_created_notifications(request):
    actions = Actions(request.user.id, settings.UNIHELP_DATABASE)
    notifications_by_state = actions.search_notification_by_state(State.CREATED)
    return render(request,
                  'notifications/index.html',
                  {'notifications': notifications_by_state,
                   'user': request.user,
                   'nav_bar': 'notifications',
                   'header': 'Created notifications',
                   'await_notifications':
                       await_notifications(request.user.id),
                   'await_notifications_count':
                       len(await_notifications(request.user.id))})


@login_required
@check_notifications
def search_await_notifications(request):
    actions = Actions(request.user.id, settings.UNIHELP_DATABASE)
    notifications_by_state = actions.search_notification_by_state(State.AWAIT)
    return render(request,
                  'notifications/index.html',
                  {'notifications': notifications_by_state,
                   'user': request.user,
                   'nav_bar': 'notifications',
                   'header': 'Await notifications',
                   'view': 'await',
                   'await_notifications':
                       await_notifications(request.user.id),
                   'await_notifications_count':
                       len(await_notifications(request.user.id))})


@login_required
@check_notifications
def search_shown_notifications(request):
    actions = Actions(request.user.id, settings.UNIHELP_DATABASE)
    notifications_by_state = actions.search_notification_by_state(State.SHOWN)
    return render(request,
                  'notifications/index.html',
                  {'notifications': notifications_by_state,
                   'user': request.user,
                   'nav_bar': 'notifications',
                   'header': 'Shown notifications',
                   'await_notifications':
                       await_notifications(request.user.id),
                   'await_notifications_count':
                       len(await_notifications(request.user.id))})


@login_required
@check_notifications
def delete_notification(request, notification_id):
    if request.method == 'POST':
        actions = Actions(request.user.id, settings.UNIHELP_DATABASE)
        actions.delete_notification(notification_id)
    return redirect('unihelp:all_notifications')


@login_required
def set_shown_notification(request, notification_id):
    if request.method == 'POST':
        actions = Actions(request.user.id, settings.UNIHELP_DATABASE)
        actions.set_shown_notifications(notification_id)
    return redirect('unihelp:all_notifications')


@login_required
def repeats(request):
    actions = Actions(request.user.id, settings.UNIHELP_DATABASE)
    all_tasks = actions.search_user_tasks()
    tasks = [task for task in all_tasks if task.status == Status.REPEAT.value]
    return render(request,
                  'planners/repeats.html',
                  {'tasks': tasks,
                   'user': request.user,
                   'nav_bar': 'planners',
                   'await_notifications':
                       await_notifications(request.user.id),
                   'await_notifications_count':
                       len(await_notifications(request.user.id))})


@login_required
def create_repeat_task(request):
    if request.method == 'POST':
        form = RepeatTaskForm(request.user.id, request.POST)

        if form.is_valid():
            title = form.cleaned_data['title']
            note = form.cleaned_data['note']
            tag_id = form.cleaned_data['tag']
            priority = form.cleaned_data['priority']
            is_event = form.cleaned_data['event']
            time_start = form.cleaned_data['time_start']
            time_end = form.cleaned_data['time_end']

            if not tag_id:
                tag_id = None

            task = Task(
                title=title,
                note=note,
                tag_id=tag_id,
                priority=priority,
                status=Status.REPEAT.value,
                is_event=is_event,
                time_start=time_start,
                time_end=time_end)

            actions = Actions(request.user.id, settings.UNIHELP_DATABASE)
            task.id = actions.add_task(task).id
            readers = form.cleaned_data['readers']
            writers = form.cleaned_data['writers']
            actions.delete_all_readers(task.id)

            if readers is not None:
                ids = [user.id for user in readers]
                for user_id in ids:
                    actions.add_reader(user_id, task.id)
            actions.delete_all_writers(task.id)

            if writers is not None:
                ids = [user.id for user in writers]
                for user_id in ids:
                    actions.add_writer(user_id, task.id)

            return redirect('unihelp:repeats')
    else:
        form = RepeatTaskForm(request.user.id)

    return render(request,
                  'tasks/new.html',
                  {'form': form,
                   'user': request.user,
                   'nav_bar': 'planners',
                   'await_notifications':
                       await_notifications(request.user.id),
                   'await_notifications_count':
                       len(await_notifications(request.user.id))})


@login_required
@check_planners
def planners(request):
    actions = Actions(request.user.id, settings.UNIHELP_DATABASE)
    all_planners = actions.search_all_planners()
    return render(request,
                  'planners/index.html',
                  {'planners': all_planners,
                   'user': request.user,
                   'nav_bar': 'planners',
                   'await_notifications':
                       await_notifications(request.user.id),
                   'await_notifications_count':
                       len(await_notifications(request.user.id))})


@login_required
@check_planners
def create_planner(request):
    if request.method == 'POST':
        form = PlannerForm(request.user.id, request.POST)
        str_interval = form.data['interval']
        convert_time = dateparser.parse(str_interval)
        interval = (datetime.datetime.now() - convert_time).total_seconds()

        if interval is None:
            form.add_error('interval', "Interval is incorrect")
        elif interval < 60:
            form.add_error(
                'interval',
                "Interval should be more than 1 minute")

        if form.is_valid():
            last_repeat = form.cleaned_data['last_repeat']

            if last_repeat is None:
                last_repeat = (datetime.datetime.now()
                               - datetime.timedelta(seconds=interval))

            task_id = int(form.cleaned_data['task_repeat'])
            actions = Actions(request.user.id, settings.UNIHELP_DATABASE)

            planner = Planner(
                interval=interval,
                last_repeat=last_repeat,
                user_id=request.user.id,
                task_id=task_id)

            actions.add_planner(planner)
            return redirect('unihelp:planners')
    else:
        task_repeat = None
        if request.GET.get('task_repeat') is not None:
            task_repeat = request.GET.get('task_repeat')

        form = PlannerForm(request.user.id)
        if task_repeat is not None:
            form.fields['task_repeat'].initial = task_repeat

    return render(request,
                  'planners/new.html',
                  {'form': form,
                   'user': request.user,
                   'nav_bar': 'planners',
                   'await_notifications':
                       await_notifications(request.user.id),
                   'await_notifications_count':
                       len(await_notifications(request.user.id))})


@login_required
@check_planners
def edit_planner(request, planner_id):
    actions = Actions(request.user.id, settings.UNIHELP_DATABASE)
    planner = actions.search_planner(planner_id)
    if planner is None:
        return redirect('unihelp:planners')

    if request.method == 'POST':
        form = PlannerForm(request.user.id, request.POST)
        str_interval = form.data['interval']
        convert_time = dateparser.parse(str_interval)
        interval = (datetime.datetime.now()
                    - convert_time).total_seconds()

        if interval is None:
            form.add_error('interval', "Interval is incorrect")

        elif interval < 60:
            form.add_error(
                'interval',
                "Interval should be more than 1 minute")

        if form.is_valid():
            last_repeat = form.cleaned_data['last_repeat']

            if last_repeat is None:
                last_repeat = (datetime.datetime.now()
                               - datetime.timedelta(seconds=interval))

            planner.last_repeat = last_repeat
            planner.interval = interval
            actions.update_planner(planner)
            return redirect('unihelp:plans')
    else:
        form = PlannerForm(request.user.id,
                           initial={'last_repeat': planner.last_repeat,
                                    'interval': datetime.timedelta(
                                        seconds=planner.interval)})
    return render(request,
                  'planners/edit.html',
                  {'form': form,
                   'user': request.user,
                   'nav_bar': 'planners',
                   'await_notifications':
                       await_notifications(request.user.id),
                   'await_notifications_count':
                       len(await_notifications(request.user.id))})


@login_required
@check_planners
def delete_planner(request, planner_id):
    if request.method == 'POST':
        actions = Actions(request.user.id, settings.UNIHELP_DATABASE)
        actions.delete_planner(planner_id)
    return redirect('unihelp:planners')
