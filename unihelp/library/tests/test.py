import time
import unittest
import datetime

from unihelplib.storage.mediator import (
    BaseMediator,
    TaskMediator,
    TagMediator,
    PlannerMediator,
    NotificationMediator)
from unihelplib.models.models import (
    Tag,
    Task,
    Planner,
    Notification,
    State)
from unihelplib.actions import Actions


class TestMediator(unittest.TestCase):
    def setUp(self):
        self.database = 'Vasya/Vasya@localhost:1521/XE'

        self.user_id = 21

        self.tag_wrapper = TagMediator(
            self.user_id,
            self.database)
        self.task_wrapper = TaskMediator(
            self.user_id,
            self.database)
        self.planner_wrapper = PlannerMediator(
            self.user_id,
            self.database)
        self.notification_wrapper = NotificationMediator(
            self.user_id,
            self.database)
        self.base_mediator = BaseMediator(
            self.user_id,
            self.database)

        self.tag = Tag(name="pony")
        self.tag_wrapper.create(self.tag)
        self.task = Task(title="pony", note="pony", user_id=21)
        self.task_wrapper.create(self.task)

        self.planner = Planner(
            interval=500,
            user_id=21,
            task_id=self.task.id,
            last_repeat=datetime.datetime.now())
        self.notification = Notification(
            title="pony",
            user_id=21,
            work_start_time=datetime.datetime.now())

    def test(self):
        before = self.base_mediator.counter_table("Planner")
        self.planner_wrapper.create(self.planner)
        self.notification_wrapper.create(self.notification)
        after = self.base_mediator.counter_table("Planner")
        self.assertEqual(before + 1, after)

        tag_id = self.tag_wrapper.create(self.tag)
        tag_db = self.tag_wrapper.search(tag_id.id)

        task_id = self.task_wrapper.create(self.task)
        task_db = self.task_wrapper.search(task_id.id)
        self.assertEqual(task_id.id, task_db.id)
        self.assertEqual(tag_id.id, tag_db.id)
        self.assertEqual(tag_id.user_id, tag_db.user_id)

        tag_id = self.tag_wrapper.create(self.tag)
        self.tag_wrapper.delete(tag_id.id)
        self.assertEqual(self.tag_wrapper.search(tag_id.id), None)

        self.tag_wrapper.create(self.tag)
        planner_id = self.planner_wrapper.create(self.planner)
        planner_db = self.planner_wrapper.search(planner_id.id)
        self.assertEqual(planner_id.id, planner_db.id)
        self.notification_wrapper.create(self.notification)

        task_with_id = self.task_wrapper.create(self.task)
        task_with_id.title = "Magic"
        self.task_wrapper.update(task_with_id)
        update = self.task_wrapper.search(task_with_id.id)
        self.assertEqual(task_with_id.title, update.title)

        task_id = self.task_wrapper.create(self.task).id
        subtask = self.task_wrapper.create_subtask(task_id, self.task)
        self.assertEqual(subtask.parent_task_id, task_id)


class TestActions(unittest.TestCase):
    def setUp(self):
        self.database = 'Vasya/Vasya@localhost:1521/XE'
        self.user_id = 21

        self.actions = Actions(self.user_id, self.database)
        self.base_mediator = BaseMediator(
            self.user_id,
            self.database)

        self.task = Task(title="witch")
        self.tag = Tag(name="friendship")
        self.planner = Planner(
            interval=500)
        self.notification = Notification(
            title="unicorn")


    def test(self):
        before = self.base_mediator.counter_table("Tag")
        tag = self.actions.add_tag(self.tag)
        time.sleep(2)
        after = self.base_mediator.counter_table("Tag")
        self.assertEqual(before + 1, after)

        before = self.base_mediator.counter_table("Notification")
        notification = self.actions.add_notification(self.notification)
        time.sleep(2)
        self.actions.search_notification_by_state(State.CREATED)
        after = self.base_mediator.counter_table("Notification")
        self.assertEqual(before + 1, after)

        before = self.base_mediator.counter_table("Task")
        task = self.actions.add_task(self.task)
        time.sleep(2)
        after = self.base_mediator.counter_table("Task")
        self.assertEqual(before + 1, after)

        before = self.base_mediator.counter_table("Planner")
        planner = self.actions.add_planner(self.planner)
        time.sleep(2)
        after = self.base_mediator.counter_table("Planner")
        self.assertEqual(before + 1, after)

        mediator = TaskMediator(user_id=2, db_name="SYSTEM/Apz3cmcm@localhost:1522/XE")
        mediator.add_reader(user_id=8, task_id=2)
        mediator.add_writer(user_id=8, task_id=2)
        time.sleep(2)


        self.assertEqual(self.actions.check_user_is_reader(task.id), True)
        self.assertEqual(self.actions.check_user_is_writer(task.id), True)

        self.assertEqual(self.actions.search_task(task.id).id, task.id)
        self.assertEqual(self.actions.search_task(task.id).time_create, task.time_create)
        self.assertEqual(self.actions.search_task(task.id).time_update, task.time_update)

        self.assertEqual(self.actions.search_tag(tag.id).id, tag.id)
        self.assertEqual(self.actions.search_planner(planner.id).id, planner.id)
        self.assertEqual(self.actions.search_notification(
            notification.id).id, notification.id)

        self.actions.set_task_status(task.id, 3)
        time.sleep(2)
        task = self.actions.search_task(task.id)
        self.assertEqual(task.status, 3)

        self.actions.add_reader(11, task.id)
        self.actions.add_writer(11, task.id)

        self.actions.delete_reader(11, task.id)
        self.actions.delete_writer(11, task.id)

        child = self.actions.create_subtask(task.id, self.task)
        self.assertEqual(self.actions.search_parent_task(child.id).id, task.id)

        before = self.base_mediator.counter_table("Task")
        self.actions.delete_task(task.id)
        self.actions.delete_task(child.id)
        after = self.base_mediator.counter_table("Task")
        self.assertEqual(before - 2, after)

        before = self.base_mediator.counter_table("Tag")
        self.actions.delete_tag(tag.id)
        after = self.base_mediator.counter_table("Tag")
        self.assertEqual(before - 1, after)

        before = self.base_mediator.counter_table("Planner")
        self.actions.delete_planner(planner.id)
        after = self.base_mediator.counter_table("Planner")
        self.assertEqual(before - 1, after)

        before = self.base_mediator.counter_table("Notification")
        self.actions.delete_notification(notification.id)
        after = self.base_mediator.counter_table("Notification")
        self.assertEqual(before - 1, after)


if __name__ == "__main__":
    unittest.main()
