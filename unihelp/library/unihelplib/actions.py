"""This module describes the interaction with the unihelplibrary and the logger.
Describes possible interactions between library models.
"""
import datetime
import dateparser

from unihelplib.models.models import Status, Planner
from unihelplib.storage.mediator import (
    TaskMediator,
    TagMediator,
    PlannerMediator,
    BaseMediator,
    NotificationMediator)

from unihelplib.exceptions.exceptions import (
    InvalidTaskTimeError,
    InvalidTaskIntervalError,
    TaskDoesntExistError,
    AccessRightsError)

from unihelplib import logger


def check_task(task):
    if task.time_start is not None and task.time_end is not None:
        if task.time_start > task.time_end:
            raise InvalidTaskTimeError(task.time_start, task.time_end)


def check_planner(planner):
    """The interval should be greater than 60 seconds."""
    if planner.interval < 60:
        raise InvalidTaskIntervalError(planner.interval)


class Actions:
    def __init__(self, user_id, db_name="SYSTEM/Apz3cmcm@localhost:1522/XE"):
        self.task_mediator = TaskMediator(user_id, db_name)
        self.tag_mediator = TagMediator(user_id, db_name)
        self.planner_mediator = PlannerMediator(user_id, db_name)
        self.notification_mediator = NotificationMediator(user_id, db_name)
        self.base_mediator = BaseMediator(user_id, db_name)

    def create_user(self, username, password):
        self.base_mediator.create_user(username, password)

    def progress_planners(self):
        """Check and update time for periodic task."""
        '''planners = []
        for row in self.base_mediator.cursor.execute("SELECT * FROM Planner"):
            planners.append(Planner(id=row['id'],
                              user_id=row['user_id'],
                              task_id=row['task_id'],
                              interval=row['interval'],
                              last_repeat=dateparser.parse(row['last_repeat'])))

        for planner in planners:
            last_repeat = planner.last_repeat
            if last_repeat < datetime.datetime.now():
                try:
                    task = self.task_mediator.search(planner.task_id)
                    task.id = None
                    task.status = Status.IN_PROGRESS.value
                    task.planner_id = planner.id
                    self.add_task(task)

                    time = datetime.timedelta(seconds=planner.interval)
                    while last_repeat < datetime.datetime.now():
                        last_repeat += time

                    planner.last_repeat = last_repeat
                    self.planner_mediator.update(planner)
                except:
                    return None'''
        self.planner_mediator.progress(self.task_mediator)

    def check_user_is_reader(self, task_id):
        """Looks up the task in the database
        and checks the user's rights to read it.
        """
        task = self.task_mediator.search(task_id)
        if task is not None:
            return (task.user_id == self.task_mediator.user_id or
                    self.task_mediator.check_is_reader(task_id))

    def check_user_is_writer(self, task_id):
        """Looks up the task in the database
            and checks the user's rights to write it.
        """
        task = self.task_mediator.search(task_id)
        if task is not None:
            return (task.user_id == self.task_mediator.user_id or
                    self.task_mediator.check_is_writer(task_id))

    def add_task(self, task):
        """Verifies the task and adds the user."""
        check_task(task)
        logger.get_logger().info(
            "Task added")
        return self.task_mediator.create(task)

    def delete_task(self, task_id):
        """Сhecks the permissions and deletes
         the task from the database.
         """
        if self.check_user_is_writer(task_id):
            self.task_mediator.delete(task_id)
            logger.get_logger().info(
                "Task deleted ID:{}".format(task_id))
        else:
            logger.get_logger().error("User doesn't have the right.")
            raise AccessRightsError

    def update_task(self, task):
        """Checks new data, permissions, and updates the task."""
        check_task(task)
        if self.check_user_is_writer(task.id):
            self.task_mediator.update(task)
            logger.get_logger().info("Updated task ID:{}".format(task.id))
        else:
            logger.get_logger().error("User doesn't gae rights.")
            raise AccessRightsError

    def search_task(self, task_id):
        """Verifies the user's rights and seeks a task."""
        if (self.check_user_is_writer(task_id) or
                self.check_user_is_reader(task_id)):
            return self.task_mediator.search(task_id)
        else:
            return None

    def add_planner(self, planner):
        """Checks the planner and adds to the user."""
        check_planner(planner)
        logger.get_logger().info("Planner added")
        return self.planner_mediator.create(planner)

    def delete_planner(self, planner_id):
        """Deletes planner from base."""
        self.planner_mediator.delete(planner_id)
        logger.get_logger().info("Planner deleted ID:{}".format(planner_id))

    def update_planner(self, planner):
        """Checks the new data and updates the existing planner."""
        check_planner(planner)
        self.planner_mediator.update(planner)
        logger.get_logger().info("Planner updated ID:{}".format(planner.id))

    def search_planner(self, planner_id):
        """Search in database."""
        return self.planner_mediator.search(planner_id)

    def search_tasks_by_planner(self, planner_id):
        """Searches for all the user's planned tasks for the planner's ID."""
        return self.task_mediator.search_tasks_by_planner(planner_id)

    def search_all_planners(self):
        """Search all planners by user."""
        return self.planner_mediator.search_all()

    def create_subtask(self, parent_task_id, task):
        """Verifies the user's rights on the parent task
        and creates the children task.
        """
        parent_task = self.search_task(parent_task_id)
        if parent_task is None:
            logger.get_logger().error("Task doesn't exist.")
            raise TaskDoesntExistError

        check_task(task)
        if self.check_user_is_writer(parent_task_id):
            logger.get_logger().info(
                "Created subtask for parent task ID:{}".format(task.id))
            return self.task_mediator.create_subtask(parent_task_id, task)
        else:
            logger.get_logger().error("User doesn't have rights.")
            raise AccessRightsError

    def search_subtasks(self, task_id, recursive=False):
        """Search subtask(recursive=False) or subtasks(recursive=True) for task."""
        if self.check_user_is_writer(task_id):
            return self.task_mediator.search_subtasks(task_id, recursive)
        else:
            logger.get_logger().error("User doesn't have rights.")
            raise AccessRightsError

    def search_parent_task(self, task_id):
        task = self.search_task(task_id)
        check_1 = task is not None
        check_2 = task.parent_task_id is not None
        if check_1 and check_2:
            return self.search_task(task.parent_task_id)

    def add_reader(self, user_id, task_id):
        """Adds user-reader to task."""
        if self.check_user_is_writer(task_id):
            self.task_mediator.add_reader(user_id, task_id)
            logger.get_logger().info(
                "Added user ID:{} read right to task ID:{}".format(
                    user_id, task_id))
        else:
            logger.get_logger().error("User doesn't have rights.")
            raise AccessRightsError

    def add_writer(self, user_id, task_id):
        """Adds user-writer to task."""
        if self.check_user_is_writer(task_id):
            self.task_mediator.add_writer(user_id, task_id)
            logger.get_logger().info(
                "Added user ID:{} write right to task ID:{}".format(
                    user_id, task_id))
        else:
            logger.get_logger().error("User doesn't have rights.")
            raise AccessRightsError

    def delete_reader(self, user_id, task_id):
        """Deletes user-reader from task."""
        if self.check_user_is_writer(task_id):
            self.task_mediator.delete_reader(user_id, task_id)
            logger.get_logger().info(
                "Delete read right to task ID:{} from user ID:{}".format(
                    task_id, user_id))
        else:
            logger.get_logger().error("User doesn't have rights.")
            raise AccessRightsError

    def delete_writer(self, user_id, task_id):
        """Deletes user-writer from task."""
        if self.check_user_is_writer(task_id):
            self.task_mediator.delete_writer(user_id, task_id)
            logger.get_logger().info(
                "Delete write right to task ID:{} from user ID:{}".format(
                    task_id, user_id))
        else:
            logger.get_logger().error("User doesn't have rights.")
            raise AccessRightsError

    def delete_all_readers(self, task_id):
        if self.check_user_is_writer(task_id):
            self.task_mediator.delete_all_readers(task_id)
            logger.get_logger().info(
                "Delete read right to task ID:{} for all users.".format(task_id))
        else:
            logger.get_logger().error("User doesn't have rights.")
            raise AccessRightsError

    def delete_all_writers(self, task_id):
        if self.check_user_is_writer(task_id):
            self.task_mediator.delete_all_writers(task_id)
            logger.get_logger().info(
                "Delete write right to task ID:{} for all users.".format(task_id))
        else:
            logger.get_logger().error("User doesn't have rights.")
            raise AccessRightsError

    def search_readers(self, task_id):
        return self.task_mediator.search_readers(task_id)

    def search_writers(self, task_id):
        return self.task_mediator.search_writers(task_id)

    def search_user_tasks(self):
        """Search for user-created tasks."""
        return self.task_mediator.search_user_tasks()

    def search_user_can_read_tasks(self):
        """List of user tasks with read rights."""
        return self.task_mediator.search_read_tasks()

    def search_user_can_write_tasks(self):
        """List of user tasks with write rights."""
        return self.task_mediator.search_write_tasks()

    def search_tasks_by_status(self, status):
        """Search tasks by status."""
        return self.task_mediator.search_by_status(status)

    def set_task_status(self, task_id, status):
        """class Status:

        CREATED 0 - was created;
        IN_PROGRESS 1 - in the process of implementation;
        DONE 2 - finished;
        ARCHIVED 3 - in the archive;
        REPEAT 4 - template for cyclic/repeat tasks planner.
        """
        if self.check_user_is_writer(task_id):
            self.task_mediator.set_status(task_id, status)
            logger.get_logger().info(
                "Set task ID:{} status {}".format(
                    task_id, status))
        else:
            logger.get_logger().error("User doesn't have rights.")
            raise AccessRightsError

    def add_tag(self, tag):
        """Adds tag to user."""
        logger.get_logger().info("Added tag {}.".format(tag.name))
        return self.tag_mediator.create(tag)

    def delete_tag(self, tag_id):
        """Deletes tag from user."""
        tag = self.tag_mediator.search(tag_id)
        if tag.user_id == self.tag_mediator.user_id:
            self.tag_mediator.delete(tag_id)
            logger.get_logger().info("Deleted tag ID:{}".format(tag_id))

    def search_user_tags(self):
        """User's tags."""
        return self.tag_mediator.search_all()

    def update_tag(self, tag):
        if self.tag_mediator.search(tag.id).user_id == self.tag_mediator.user_id:
            self.tag_mediator.update(tag)
            logger.get_logger().info("Tad {} updated.".format(tag.name))
        else:
            logger.get_logger().error("User doesn't have rights.")
            raise AccessRightsError

    def search_tag(self, tag_id):
        return self.tag_mediator.search(tag_id)

    def add_notification(self, notification):
        """Adds a notification."""
        if notification.task_id is not None:
            task = self.search_task(notification.task_id)
            if task is None:
                logger.get_logger().error("Task doesn't exist.")
                raise TaskDoesntExistError
        logger.get_logger().info("Added notification {}".format(notification.id))
        return self.notification_mediator.create(notification)

    def delete_notification(self, notification_id):
        """Deletes notification."""
        self.notification_mediator.delete(notification_id)
        logger.get_logger().info("Deleted notification.")

    def update_notification(self, notification):
        """Checks and updates notification."""
        if notification.taskis is not None:
            task = self.search_task(notification.task_id)
            if task is None:
                logger.get_logger().error("Task doesn't exist.")
                raise TaskDoesntExistError
        self.notification_mediator.update(notification)
        logger.get_logger().info("Updated notification {}.".format(notification.id))

    def search_notification(self, notification_id):
        return self.notification_mediator.search(notification_id)

    def search_all_notification(self):
        """Searches for all user notifications."""
        return self.notification_mediator.search_all()

    def set_shown_notifications(self, notification_id):
        """Change the statе of the notification to shown."""
        self.notification_mediator.set_state_shown(notification_id)
        logger.get_logger().info(
            "Set notification ID:{} status SHOWN.".format(notification_id))

    def search_notification_by_state(self, state):
        """class State:

        CREATED - was created;
        AWAIT - should be shown;
        SHOWN - was shown."""
        return self.notification_mediator.search_all_by_state(state)

  #  def filter_tasks(self, *args):
  #      return self.task_mediator.filter(*args)

    def search_active_tasks(self):
        return self.task_mediator.search_active_tasks(self.task_mediator.user_id)

    def search_tasks_by_tag(self, tag_id):
        return self.task_mediator.search_tasks_by_tag(self.task_mediator.user_id, tag_id)

    def search_tasks_by_status_(self, status_value):
        return self.task_mediator.search_tasks_by_status(self.task_mediator.user_id, status_value)

    def search_tasks_not_repeat(self):
        return self.task_mediator.search_tasks_not_repeat(self.task_mediator.user_id)

    def search_tasks_by_priority(self, priority_value):
        return self.task_mediator.search_tasks_by_priority(self.task_mediator.user_id, priority_value)

    def search_future_tasks(self):
        return self.task_mediator.search_future_tasks(self.task_mediator.user_id)