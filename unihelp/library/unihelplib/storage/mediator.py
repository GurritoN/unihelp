"""This module is a mediator for models in sqlitebase.

The library does not have a user model because
the user is defined at the console or web application
level. For example, the task should know about its creator
or partner. So BaseMediator need param user_id.
To work with the base optional need param database_name.
"""
import datetime
import cx_Oracle
import pytz
import dateparser
import dateutil.parser
from dateutil.tz import tzlocal
from collections import deque
from unihelplib.models.models import (
    Tag,
    Task,
    State,
    Status,
    Planner,
    Notification,
    CreateTables)


class BaseMediator:
    def __init__(self, user_id, db_name="SYSTEM/Apz3cmcm@localhost:1522/XE"):
        self.user_id = user_id
        self.db_name = db_name

        self.conn = cx_Oracle.connect(db_name)
        self.cursor = self.conn.cursor()
        self.cursor.execute("alter session set current_schema=system")

    def counter_table(self, Table):
        self.cursor.execute(f"SELECT COUNT(*) FROM {Table}")
        result = self.cursor.fetchone()[0]
        return result if result is not None else 0

    def create_user(self, username, password):
        self.cursor.execute('alter session set "_ORACLE_SCRIPT" = true')
        self.cursor.execute(f'create user {username} identified by {password}')
        self.cursor.execute(f'grant connect to {username}')
        self.cursor.execute(f'grant create session to {username}')

class TagMediator(BaseMediator):
    def create(self, tag):
        """Creat tag for user in database."""
        tag.user_id = self.user_id
        self.cursor.callproc("create_tag", [tag.name, tag.user_id])
        return tag

    def delete(self, tag_id):
        """Delete tag from database."""
        self.cursor.callproc("delete_tag", [tag_id])

    def update(self, tag):
        """Update tag in database."""
        self.cursor.callproc("update_tag", [tag.name, tag.id])
        return tag

    def search(self, tag_id):
        """Search tag by id."""
        result = self.cursor.callfunc('get_tag', cx_Oracle.CURSOR, [tag_id]).fetchone()
        if result is None:
            return None
        return Tag(id=result[0],
                   name=result[1],
                   user_id=result[2])

    def search_all(self):
        """Search all tags for user."""
        tags = []
        for row in self.cursor.callfunc('get_tag', cx_Oracle.CURSOR, [self.user_id]).fetchall():
            tags.append(Tag(id=row[0],
                            name=row[1],
                            user_id=row[2]))
        return tags


class NotificationMediator(BaseMediator):
    def create(self, notification):
        """Create notification for user in database."""
        notification.user_id = self.user_id

        self.cursor.callproc('create_notification',
                                               [notification.title,
                                                notification.state,
                                                notification.user_id,
                                                0 if notification.task_id is None else notification.task_id,
                                                str(notification.work_start_time)])
        return notification

    def delete(self, notification_id):
        """Delete notification from database."""
        self.cursor.callproc('delete_notification', [notification_id])

    def update(self, notification):
        """Update notification in database."""
        self.cursor.callproc('update_notification',
           [notification.title,
            notification.state,
            notification.user_id,
            0 if notification.task_id is None else notification.task_id,
            str(notification.work_start_time),
            notification.id])
        return notification

    def search(self, notification_id):
        """Search notification by id."""
        notification = self.cursor.callfunc('get_notification', cx_Oracle.CURSOR, [notification_id]).fetchone()
        if not notification:
            return None
        return Notification(id=notification[0],
                            title=notification[1],
                            task_id=notification[2],
                            user_id=notification[3],
                            work_start_time=dateparser.parse(
                                notification[4]),
                            state=notification[5])

    def search_all(self):
        """Search all notifications for user."""
        notifications = []
        for row in self.cursor.callfunc('get_user_notifications', cx_Oracle.CURSOR, [self.user_id]).fetchall():
            notifications.append(
                Notification(id=row[0], title=row[1],
                             task_id=row[2],
                             user_id=row[3],
                             work_start_time=dateparser.parse(
                                 row[4]),
                             state=row[5]))
        return notifications

    def search_all_by_state(self, state):
        """Search notification by param state.

        state value:
        CREATED = 0
        AWAIT = 1
        SHOWN = 2
        """
        notifications = []
        for row in self.cursor.callfunc('get_user_notifications_by_state', cx_Oracle.CURSOR, [self.user_id, state.value]).fetchall():
            notifications.append(
                Notification(id=row[0], title=row[1],
                             task_id=row[2],
                             user_id=row[3],
                             work_start_time=dateparser.parse(
                                 row[4]),
                             state=row[5]))
        return notifications

    def set_state_shown(self, notification_id):
        """Make notification.state SHOWN."""
        notification = self.search(notification_id)
        notification.state = State.SHOWN.value
        self.update(notification)

    def progress(self):
        """Make progress for notifications state.CREATED->state.AWAIT."""
        for row in self.cursor.callfunc('get_all_notifications_by_state'):
            if dateparser.parse(row[4]) < datetime.datetime.now():
                state = State.AWAIT.value
                notification = Notification(id=row[0], title=row[1],
                             task_id=row[2],
                             user_id=row[3],
                             work_start_time=dateparser.parse(
                                 row[4]),
                             state=state)
                self.update(notification)


class PlannerMediator(BaseMediator):
    def create(self, planner):
        """Create planner for user in database."""
        planner.user_id = self.user_id
        self.cursor.callproc('create_planner', [self.user_id, planner.task_id, int(planner.interval), str(planner.last_repeat)])
        return planner

    def delete(self, planner_id):
        """Delete planner from database."""
        self.cursor.callproc('delete_planner', [planner_id])

    def update(self, planner):
        """Update planner in database."""
        self.cursor.callproc('update_planner', [planner.id, int(planner.interval), str(planner.last_repeat)])
        return planner

    def search(self, planner_id):
        """Search by id in database."""
        result = self.cursor.callfunc('get_planner', cx_Oracle.CURSOR, [planner_id]).fetchone()
        if not result:
            return None
        return Planner(
            id=result[0],
            user_id=result[2],
            task_id=result[1],
            interval=result[3],
            last_repeat=dateparser.parse(result[4]))

    def search_all(self):
        """Search all planners for user."""
        planners = []
        for row in self.cursor.callfunc('get_planners_by_user', cx_Oracle.CURSOR, [self.user_id]).fetchall():
            task = None
            if row[1] != 0:
                mediator = TaskMediator(user_id=self.user_id, db_name=self.db_name)
                task = mediator.search(row[1])
            planners.append(
                Planner(id=row[0],
                        user_id=row[2],
                        task_id=task.id,
                        interval=row[3],
                        last_repeat=dateparser.parse(row[4])))
        return planners

    def progress(self, task_mediator):
        """Check and update time for periodic task."""
        for row in self.cursor.callfunc('get_all_planners').fetchall():
            task = None
            if row[1] != 0:
                task = task_mediator.search(row[1])
            planner = Planner(id=row[0],
                              user_id=row[2],
                              task_id=task.id,
                              interval=int(row[3]),
                              last_repeat=dateutil.parser.parse(row[4]).astimezone(tzlocal()))

            last_repeat = (planner.last_repeat + datetime.timedelta(seconds=planner.interval))

            now = pytz.UTC.localize(datetime.datetime.now())
            if last_repeat < now:
                try:
                    task = task_mediator.search(planner.task_id)
                    task.id = None
                    task.status = Status.IN_PROGRESS.value
                    task.planner_id = planner.id
                    task_mediator.create(task)
                    time = datetime.timedelta(seconds=(planner.interval))

                    while last_repeat < now:
                        last_repeat += time

                    planner.last_repeat = last_repeat
                    self.update(planner)
                except:
                    return None


class TaskMediator(BaseMediator):
    def create(self, task):
        """Create task for user in database."""
        task.user_id = self.user_id
        self.cursor.callproc('create_task',
            [task.title,
            task.note,
            task.user_id,
            0 if task.tag_id is None else task.tag_id,
            0 if task.planner_id is None else task.planner_id,
            0 if task.parent_task_id is None else task.parent_task_id,
            "" if task.time_start is None else str(task.time_start),
            "" if task.time_end is None else str(task.time_end),
            str(task.time_create),
            str(task.time_update),
            int(task.is_event),
            task.priority,
            task.status])

        return task

    def delete(self, task_id):
        """Delete task from database."""
        self.cursor.callproc('delete_task', [task_id])

    def update(self, task):
        """Update task in database."""
        self.cursor.callproc('update_task',
            [task.id,
            task.title,
            task.note,
            task.user_id,
            task.tag_id,
            0 if task.planner_id is None else task.planner_id,
            0 if task.parent_task_id is None else task.parent_task_id,
            "" if task.time_start is None else str(task.time_start),
            "" if task.time_end is None else str(task.time_end),
            str(task.time_create),
            str(datetime.datetime.now()),
            int(task.is_event),
            task.priority,
            task.status])
        return task

    def search(self, task_id):
        """Search task by id in database."""
        result = self.cursor.callfunc('get_task', cx_Oracle.CURSOR, [task_id]).fetchone()
        if not result:
            return None
        return Task(id=result[0],
                    title=result[1],
                    note=result[2],
                    user_id=result[7],
                    tag_id=result[10] if result[10] != 0 else None,
                    planner_id=result[8],
                    parent_task_id=result[9],
                    time_start=dateparser.parse("" if result[3] is None else result[3]),
                    time_end=dateparser.parse("" if result[4] is None else result[4]),
                    time_create=dateparser.parse(result[5]),
                    time_update=dateparser.parse(result[6]),
                    is_event=bool(result[11]),
                    priority=result[12],
                    status=result[13])

    def search_user_tasks(self):
        """Search all task for user."""
        tasks = []
        for row in self.cursor.callfunc('get_tasks_by_user', cx_Oracle.CURSOR, [self.user_id]).fetchall():
            tasks.append(Task(
                id=row[0],
                title=row[1],
                note=row[2],
                user_id=row[7],
                tag_id=row[10] if row[10] != 0 else None,
                planner_id=row[8],
                parent_task_id=row[9],
                time_start=dateparser.parse("" if row[3] is None else row[3]),
                time_end=dateparser.parse("" if row[4] is None else row[4]),
                time_create=dateparser.parse(row[5]),
                time_update=dateparser.parse(row[6]),
                is_event=bool(row[11]),
                priority=row[12],
                status=row[13]))
        return tasks

    def set_status(self, task_id, status):
        """Set status for task.

        param status:
        CREATED 0
        IN_PROGRESS 1
        DONE 2
        ARCHIVED 3
        REPEAT 4
        """
        task = self.search(task_id)
        task.status = status
        self.update(task)

    def search_by_status(self, status):
        """Get tasks by status.

        param status:
        CREATED 0
        IN_PROGRESS 1
        DONE 2
        ARCHIVED 3
        REPEAT 4
        """
        tasks = []
        for row in self.cursor.callfunc('get_tasks_by_status', cx_Oracle.CURSOR, [self.user_id, status]).fetchall():
            tasks.append(Task(
                id=row[0],
                title=row[1],
                note=row[2],
                user_id=row[7],
                tag_id=row[10] if row[10] != 0 else None,
                planner_id=row[8],
                parent_task_id=row[9],
                time_start=dateparser.parse("" if row[3] is None else row[3]),
                time_end=dateparser.parse("" if row[4] is None else row[4]),
                time_create=dateparser.parse(row[5]),
                time_update=dateparser.parse(row[6]),
                is_event=bool(row[11]),
                priority=row[12],
                status=row[13]))
        return tasks

    def create_subtask(self, parent_task_id, task):
        """Create task-children for task-parent in database."""
        task.parent_task_id = parent_task_id
        return self.create(task)

    def search_subtasks(self, task_id, recursive=False):
        """Search subtasks (single-recursive=False or
         all-recursive=True) for task."""
        if recursive:
            queue = deque()
            queue.append(self.search(task_id))
            subtasks = []
            return self.search_recursive_subtasks(queue, subtasks)
        tasks = []
        for row in self.cursor.callfunc('get_tasks_by_parent', cx_Oracle.CURSOR, [task_id]).fetchall():
            tasks.append(Task(id=row[0],
                title=row[1],
                note=row[2],
                user_id=row[7],
                tag_id=row[10] if row[10] != 0 else None,
                planner_id=row[8],
                parent_task_id=row[9],
                time_start=dateparser.parse("" if row[3] is None else row[3]),
                time_end=dateparser.parse("" if row[4] is None else row[4]),
                time_create=dateparser.parse(row[5]),
                time_update=dateparser.parse(row[6]),
                is_event=bool(row[11]),
                priority=row[12],
                status=row[13]))
        return tasks

    def search_recursive_subtasks(self, queue, subtasks):
        """BFS - breadth-first search.

        Let's create a queue q, in which the burning
        vertices will be placed, and also we will create
        an array subtasks [], in which we will mark
        for each vertex whether it is already lit or
        not (or in other words, whether it was a visitor).
        """
        if not queue:
            return subtasks
        task = queue.popleft()
        for subtask in self.search_subtasks(task.id):
            subtasks.append(subtask)
            queue.append(subtask)
        return self.search_recursive_subtasks(queue, subtasks)

    def search_tasks_by_planner(self, planner_id):
        """Search cycle tasks in planner for user."""
        tasks = []
        for row in self.cursor.callfunc('get_tasks_by_planner', cx_Oracle.CURSOR, [self.user_id, planner_id]).fetchall():
            tasks.append(Task(
                id=row[0],
                title=row[1],
                note=row[2],
                user_id=row[7],
                tag_id=row[10] if row[10] != 0 else None,
                planner_id=row[8],
                parent_task_id=row[9],
                time_start=dateparser.parse("" if row[3] is None else row[3]),
                time_end=dateparser.parse("" if row[4] is None else row[4]),
                time_create=dateparser.parse(row[5]),
                time_update=dateparser.parse(row[6]),
                is_event=bool(row[11]),
                priority=row[12],
                status=row[13]))
        return tasks

    def check_is_reader(self, task_id):
        """Check right fot read task for user."""
        return self.cursor.callfunc('check_read_rights', cx_Oracle.BOOLEAN, [self.user_id, task_id])

    def add_reader(self, user_id, task_id):
        """Add read right for task to user."""
        if not self.cursor.callfunc('check_read_rights', cx_Oracle.BOOLEAN, [self.user_id, task_id]):
            self.cursor.callproc('add_reader', [user_id, task_id])

    def search_readers(self, task_id):
        """All readers task in database."""
        users_id = []
        for row in self.cursor.callfunc('get_read_rights', cx_Oracle.CURSOR, [task_id]).fetchall():
            users_id.append(row[1])
        return users_id

    def delete_reader(self, user_id, task_id):
        """Delete read right for task."""
        self.cursor.callproc('delete_reader', [user_id, task_id])

    def delete_all_readers(self, task_id):
        """Delete all readers for task."""
        self.cursor.callproc('delete_all_readers', [task_id])

    def search_read_tasks(self):
        """Get tasks for user who can read."""
        tasks = []
        for row in self.cursor.callfunc('get_read_tasks', cx_Oracle.CURSOR, [self.user_id]).fetchall():
            tasks.append(Task(
                id=row[0],
                title=row[1],
                note=row[2],
                user_id=row[7],
                tag_id=row[10] if row[10] != 0 else None,
                planner_id=row[8],
                parent_task_id=row[9],
                time_start=dateparser.parse("" if row[3] is None else row[3]),
                time_end=dateparser.parse("" if row[4] is None else row[4]),
                time_create=dateparser.parse(row[5]),
                time_update=dateparser.parse(row[6]),
                is_event=bool(row[11]),
                priority=row[12],
                status=row[13]))
        return tasks

    def check_is_writer(self, task_id):
        """Check right fot write task for user."""
        return self.cursor.callfunc('check_write_rights', cx_Oracle.BOOLEAN, [self.user_id, task_id])

    def add_writer(self, user_id, task_id):
        """Add write right for task to user."""
        if not self.cursor.callfunc('check_write_rights', cx_Oracle.BOOLEAN, [self.user_id, task_id]):
            self.cursor.callproc('add_writer', [user_id, task_id])

    def search_writers(self, task_id):
        """All writers task in database."""
        users_id = []
        for row in self.cursor.callfunc('get_write_rights', cx_Oracle.CURSOR, [task_id]).fetchall():
            users_id.append(row[1])
        return users_id

    def delete_writer(self, user_id, task_id):
        """Delete write right for task."""
        self.cursor.callproc('delete_writer', [user_id, task_id])

    def delete_all_writers(self, task_id):
        """Delete all writers for task."""
        self.cursor.callproc('delete_all_writers', [task_id])

    def search_write_tasks(self):
        """Get tasks for user who can write."""
        tasks = []
        for row in self.cursor.callfunc('get_write_tasks', cx_Oracle.CURSOR, [self.user_id]).fetchall():
            tasks.append(Task(
                id=row[0],
                title=row[1],
                note=row[2],
                user_id=row[7],
                tag_id=row[10] if row[10] != 0 else None,
                planner_id=row[8],
                parent_task_id=row[9],
                time_start=dateparser.parse("" if row[3] is None else row[3]),
                time_end=dateparser.parse("" if row[4] is None else row[4]),
                time_create=dateparser.parse(row[5]),
                time_update=dateparser.parse(row[6]),
                is_event=bool(row[11]),
                priority=row[12],
                status=row[13]))
        return tasks

    def search_active_tasks(self, user_id):
        tasks = []
        for row in self.cursor.callfunc('get_active_tasks', cx_Oracle.CURSOR, [user_id]).fetchall():
            tasks.append(Task(
                id=row[0],
                title=row[1],
                note=row[2],
                user_id=row[7],
                tag_id=row[10] if row[10] != 0 else None,
                planner_id=row[8],
                parent_task_id=row[9],
                time_start=dateparser.parse("" if row[3] is None else row[3]),
                time_end=dateparser.parse("" if row[4] is None else row[4]),
                time_create=dateparser.parse(row[5]),
                time_update=dateparser.parse(row[6]),
                is_event=bool(row[11]),
                priority=row[12],
                status=row[13]))
        return tasks

    def search_tasks_by_tag(self, user_id, tag_id):
        tasks = []
        for row in self.cursor.callfunc('get_tasks_by_tag', cx_Oracle.CURSOR, [user_id, tag_id]).fetchall():
            tasks.append(Task(
                id=row[0],
                title=row[1],
                note=row[2],
                user_id=row[7],
                tag_id=row[10] if row[10] != 0 else None,
                planner_id=row[8],
                parent_task_id=row[9],
                time_start=dateparser.parse("" if row[3] is None else row[3]),
                time_end=dateparser.parse("" if row[4] is None else row[4]),
                time_create=dateparser.parse(row[5]),
                time_update=dateparser.parse(row[6]),
                is_event=bool(row[11]),
                priority=row[12],
                status=row[13]))
        return tasks

    def search_tasks_by_status(self, user_id, status_value):
        tasks = []
        for row in self.cursor.callfunc('get_tasks_by_status', cx_Oracle.CURSOR, [user_id, status_value]).fetchall():
            tasks.append(Task(
                id=row[0],
                title=row[1],
                note=row[2],
                user_id=row[7],
                tag_id=row[10] if row[10] != 0 else None,
                planner_id=row[8],
                parent_task_id=row[9],
                time_start=dateparser.parse("" if row[3] is None else row[3]),
                time_end=dateparser.parse("" if row[4] is None else row[4]),
                time_create=dateparser.parse(row[5]),
                time_update=dateparser.parse(row[6]),
                is_event=bool(row[11]),
                priority=row[12],
                status=row[13]))
        return tasks

    def search_tasks_not_repeat(self, user_id):
        tasks = []
        for row in self.cursor.callfunc('get_not_repeating_tasks', cx_Oracle, [user_id]).fetchall():
            tasks.append(Task(
                id=row[0],
                title=row[1],
                note=row[2],
                user_id=row[7],
                tag_id=row[10] if row[10] != 0 else None,
                planner_id=row[8],
                parent_task_id=row[9],
                time_start=dateparser.parse("" if row[3] is None else row[3]),
                time_end=dateparser.parse("" if row[4] is None else row[4]),
                time_create=dateparser.parse(row[5]),
                time_update=dateparser.parse(row[6]),
                is_event=bool(row[11]),
                priority=row[12],
                status=row[13]))
        return tasks

    def search_tasks_by_priority(self, user_id, priority_value):
        tasks = []
        for row in self.cursor.callfunc('get_tasks_by_priority', cx_Oracle.CURSOR, [user_id, priority_value]).fetchall():
            tasks.append(Task(
                id=row[0],
                title=row[1],
                note=row[2],
                user_id=row[7],
                tag_id=row[10] if row[10] != 0 else None,
                planner_id=row[8],
                parent_task_id=row[9],
                time_start=dateparser.parse("" if row[3] is None else row[3]),
                time_end=dateparser.parse("" if row[4] is None else row[4]),
                time_create=dateparser.parse(row[5]),
                time_update=dateparser.parse(row[6]),
                is_event=bool(row[11]),
                priority=row[12],
                status=row[13]))
        return tasks

    def search_future_tasks(self, user_id):
        tasks = []
        for row in self.cursor.callfunc('get_tasks_by_user', cx_Oracle.CURSOR, [user_id]).fetchall():
            if dateparser.parse(row[3]) > datetime.datetime.now():
                tasks.append(Task(
                    id=row[0],
                    title=row[1],
                    note=row[2],
                    user_id=row[7],
                    tag_id=row[10] if row[10] != 0 else None,
                    planner_id=row[8],
                    parent_task_id=row[9],
                    time_start=dateparser.parse("" if row[3] is None else row[3]),
                    time_end=dateparser.parse("" if row[4] is None else row[4]),
                    time_create=dateparser.parse(row[5]),
                    time_update=dateparser.parse(row[6]),
                    is_event=bool(row[11]),
                    priority=row[12],
                    status=row[13]))
        return tasks
